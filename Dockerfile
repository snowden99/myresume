FROM nginx:alpine as hugo_build
RUN apk update && apk add hugo 
COPY . /app
WORKDIR /app
RUN hugo 

FROM nginx:alpine
EXPOSE 80
COPY --from=hugo_build /app/public /usr/share/nginx/html

WORKDIR /usr/share/nginx/html
