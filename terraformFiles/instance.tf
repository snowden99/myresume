resource "aws_security_group" "my-sec" {

	name="my-sec"
	description="Security group to allow hhtp and ssh"

	ingress{	
		from_port=22
		to_port=22
		protocol= "tcp"
		cidr_blocks=["0.0.0.0/0"]
	}

	ingress{

		from_port=80
		to_port=80
		protocol="tcp"
		cidr_blocks=["0.0.0.0/0"]
	}
	egress{
		from_port=0
		to_port=0
		protocol="-1"
		cidr_blocks=["0.0.0.0/0"]

	}

}
resource "aws_instance" "webserver" {
  ami           = "ami-00ddb0e5626798373"
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  security_groups = ["${aws_security_group.my-sec.name}"]
  key_name= "terraform-ec2"
  tags = {
	Name="web-server"
}
}

