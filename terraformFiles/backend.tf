terraform {
  backend "s3" {
    bucket = "terraform-resume"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
